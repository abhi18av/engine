// Written by http://xojoc.pw. Public Domain.

/*
 Package loop provides a fixed time game loop for GNU/Linux.
*/
package loop

import (
	"image/draw"
	"log"
	"math/rand"
	"runtime"
	"time"

	"gitlab.com/xojoc/engine/window"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Run calls win.Create then starts a fixed-time loop. It calls win.ProcessEvents, update and render fps times in a second and then sleeps until the next iteration. To stop the loop simply destroy the window (win.Destroy).
func Run(win *window.Window, update func(dt float64), render func(img draw.Image), fps int) error {
	if update == nil {
		update = func(float64) {}
	}
	if render == nil {
		render = func(draw.Image) {}
	}
	err := win.Create()
	if err != nil {
		return err
	}
	runtime.GC()
	frameTime := int(time.Second) / fps
	for {
		t := time.Now()
		win.ProcessEvents()
		if win.Closed {
			break
		}
		update(1.0 / float64(fps))
		if win.Closed {
			break
		}
		render(win.Screen)
		if win.Closed {
			break
		}
		win.Flush()

		now := time.Now()
		diff := t.Add(time.Duration(frameTime)).Sub(now)
		if int64(diff) < 0 {
			log.Println("lagging behind: ", diff)
		}
		time.Sleep(diff)
	}
	return nil
}
