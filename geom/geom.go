// Written by http://xojoc.pw. Public Domain.

/*
 Package geom provides common geometric figure for games.
*/
package geom

import (
	"fmt"
	"image"
	"image/color"
	"math"

	"gitlab.com/xojoc/util"
)

// FIXME: Pt image.Image interface?
// FIXME: Pt.Rand Rt.Rand
// FIXME: circle
// FIXME: Rt.Fit
// FIXME: collision, polyline
// FIXME: cmplx

type Pt struct {
	X float64
	Y float64
}

// ZP is the zero Point.
var ZP Pt

func RandPt(minx, maxx, miny, maxy float64) Pt {
	return Pt{util.RandRange(minx, maxx), util.RandRange(miny, maxy)}
}

// Point returns an image.Point with rounded X and Y.
func (a Pt) Point() image.Point {
	return image.Point{int(a.X + 0.5), int(a.Y + 0.5)}
}

func (a Pt) Add(b Pt) Pt {
	return Pt{a.X + b.X, a.Y + b.Y}
}
func (a Pt) Sub(b Pt) Pt {
	return Pt{a.X - b.X, a.Y - b.Y}
}
func (a Pt) Mul(b float64) Pt {
	return Pt{a.X * b, a.Y * b}
}
func (a Pt) Div(b float64) Pt {
	return Pt{a.X / b, a.Y / b}
}

func (a Pt) Mod(b Rt) Pt {
	a = a.Sub(b.Pt)
	a.X = math.Mod(a.X, b.W)
	if a.X < 0 {
		a.X += b.W
	}
	a.Y = math.Mod(a.Y, b.H)
	if a.Y < 0 {
		a.Y += b.H
	}
	return a.Add(b.Pt)
}

func (a Pt) In(b Rt) bool {
	return !b.Empty() &&
		a.X >= b.X && a.Y >= b.Y &&
		a.X < (b.X+b.W) && a.Y < (b.Y+b.H)
}

func (a Pt) String() string {
	return fmt.Sprint("(", a.X, ",", a.Y, ")")
}

type Rt struct {
	Pt
	W float64
	H float64
}

var ZR Rt

func RandRt(minx, maxx, miny, maxy, minw, maxw, minh, maxh float64) Rt {
	return Rt{RandPt(minx, maxx, miny, maxy), util.RandRange(minw, maxw), util.RandRange(minh, maxh)}
}

func (a Rt) Add(b Pt) Rt {
	a.Pt = a.Pt.Add(b)
	return a
}

func (a Rt) Sub(b Pt) Rt {
	a.Pt = a.Pt.Sub(b)
	return a
}

func (a Rt) Empty() bool {
	return a.W <= 0 || a.H <= 0
}

func (a Rt) Eq(b Rt) bool {
	return a == b || a.Empty() && b.Empty()
}

func (a Rt) In(b Rt) bool {
	if a.Empty() {
		return true
	}
	return a.X >= b.X &&
		a.Y >= b.Y &&
		(a.X+a.W) < (b.X+b.W) &&
		(a.Y+a.H) < (b.Y+b.H)
}

func (a Rt) Overlaps(b Rt) bool {
	return !a.Empty() && !b.Empty() &&
		(a.X < b.X+b.W) && (b.X < a.X+a.W) &&
		(a.Y < b.Y+b.H) && (b.Y < a.Y+a.H)
}

func (a Rt) Union(b Rt) Rt {
	if a.Empty() {
		return b
	}
	if b.Empty() {
		return a
	}
	maxx := math.Max(a.X+a.W, b.X+b.W)
	maxy := math.Max(a.Y+a.H, b.Y+b.H)
	a.X = math.Min(a.X, b.X)
	a.Y = math.Min(a.Y, b.Y)
	a.W = maxx - a.X
	a.H = maxy - a.Y
	return a
}

func (a Rt) Intersect(b Rt) Rt {
	minx := math.Min(a.X+a.W, b.X+b.W)
	miny := math.Min(a.Y+a.H, b.Y+b.H)
	a.X = math.Max(a.X, b.X)
	a.Y = math.Max(a.Y, b.Y)
	a.W = minx - a.X
	a.H = miny - a.Y
	return a
}

// Inset the rectangle by (dx,dy). If dx is positive, then the sides are moved inwards by dx, making the rectangle narrower. If dx is negative, then the sides are moved outwards, making the rectangle wider. The same holds true for dy and the top and bottom.
func (a Rt) Inset(dx, dy float64) Rt {
	a.X += dx
	a.W -= dx
	a.Y += dy
	a.H -= dy
	// FIXME: center if empty rect?
	return a
}

func (a Rt) Center() Pt {
	if a.Empty() {
		return Pt{a.X, a.Y}
	}
	return Pt{a.X + a.W/2, a.Y + a.H/2}
}

// At implements the image.Image interface.
func (a Rt) At(x, y int) color.Color {
	if (Pt{float64(x), float64(y)}).In(a) {
		return color.Opaque
	}
	return color.Transparent
}

// Bounds implements the image.Image interface.
func (a Rt) Bounds() image.Rectangle {
	if a.Empty() {
		return image.ZR
	}
	x := int(a.X + 0.5)
	y := int(a.Y + 0.5)
	return image.Rect(x, y, x+int(a.W+0.5), y+int(a.H+0.5))
}

// ColorModel implements the image.Image interface.
func (a Rt) ColorModel() color.Model {
	return color.Alpha16Model
}

func (a Rt) String() string {
	return fmt.Sprint("(", a.X, ",", a.Y, ")-(", a.W, ",", a.H, ")")
}
