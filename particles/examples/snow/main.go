// Written by http://xojoc.pw. Public Domain.

package main

import (
	"flag"
	"image"
	"image/color"
	"image/draw"
	"log"
	"os"
	"runtime/pprof"

	//	"gitlab.com/xojoc/engine/easing"
	"gitlab.com/xojoc/engine/geom"
	"gitlab.com/xojoc/engine/inter"
	"gitlab.com/xojoc/engine/loop"
	"gitlab.com/xojoc/engine/particles"
	"gitlab.com/xojoc/engine/window"
	"gitlab.com/xojoc/util"
)

const (
	screenw = 500
	screenh = 500
)

// http://blog.golang.org/profiling-go-programs
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	groundSnow := particles.Emitter{}

	snowFlakes := particles.Emitter{
		EmissionRate: 700,
		NewParticle: func(p *particles.Particle) {
			p.Rt = geom.Rt{geom.RandPt(0, screenw, 0, 0),
				util.RandRange(1, 3), util.RandRange(1, 3)}
			p.Life = util.RandRange(1.5, 5)
			p.Velocity = geom.RandPt(-100, 100, 150, 300)
			p.Image = image.White
		},
		UpdateParticle: func(p *particles.Particle) {
			if p.Rt.Y >= screenh-p.Rt.H {
				groundSnow.Add(&particles.Particle{
					Age:   p.Age,
					Life:  p.Life,
					Rt:    p.Rt,
					Image: p.Image,
				})
				p.Destroy()
				return
			}
			p.Mask = &image.Uniform{color.Alpha{uint8(inter.Pow(2)(p.Y, 0xff, 0, screenh))}}
		},
	}

	win := window.Window{
		Width:  screenw,
		Height: screenh,
		Title:  "Falling snow :)",
	}
	update := func(dt float64) {
		if len(win.Keys) > 0 {
			win.Destroy()
		}
		snowFlakes.Update(dt)
		groundSnow.Update(dt)
	}
	render := func(img draw.Image) {
		draw.Draw(img, img.Bounds(), image.Black, image.ZP, draw.Src)
		snowFlakes.Render(img)
		groundSnow.Render(img)
	}
	util.Fatal(loop.Run(&win, update, render, 60))
}
